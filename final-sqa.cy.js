describe('FINAL-JOSE', () => {
    
  Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  })

  it('NoRegistrado',()=>{
      cy.visit('localhost:70/consultas.php')
      cy.get('[name=cui]').type('1234567891012')
      cy.get('[name=Submit]').click()
  })

  it('DatosIncorrectos+',()=>{
      cy.visit('localhost:70/consultas.php')
      cy.get('[name=cui]').type('557787887160300000')
      cy.get('[name=Submit]').click()
  })

  it('DatosLetras-Numeros',()=>{
      cy.visit('localhost:70/consultas.php')
      cy.get('[name=cui]').type('55778788jose')
      cy.get('[name=Submit]').click()
  })

  it('DatosIncorrectos-',()=>{
      cy.visit('localhost:70/consultas.php')
      cy.get('[name=cui]').type('123')
      cy.get('[name=Submit]').click()
  })
  it('DatosLetras',()=>{
      cy.visit('localhost:70/consultas.php')
      cy.get('[name=cui]').type('jose')
      cy.get('[name=Submit]').click()
  })
  
  it('SinDatos',()=>{
      cy.visit('localhost:70/consultas.php')
      cy.get('[name=cui]').type(' ')
      cy.get('[name=Submit]').click()
  })

  it('ConSimbolos',()=>{
      cy.visit('localhost:70/consultas.php')
      cy.get('[name=cui]').type('$#-/')
      cy.get('[name=Submit]').click()
  })

  it('IngresoCorrecto',()=>{
      cy.visit('localhost:70/consultas.php')
      cy.get('[name=cui]').type('2125906850101')
      cy.get('[name=Submit]').click()
  })

})