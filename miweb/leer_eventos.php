<?php
	require 'index.php';
	require 'libreria/PHPExcel/IOFactory.php';
	require 'conexiondb.php';
	$nombreArchivo = 'excel/basedatos.xls';
	$objPHPExcel = PHPEXCEL_IOFactory::load($nombreArchivo);
	$objPHPExcel->setActiveSheetIndex(6);
	$numRows = $objPHPExcel->setActiveSheetIndex(6)->getHighestRow();
	echo '<table border=1><tr><th>id</th><th>evento</th><th>inicio</th><th>fin</th><th>objetivo</th><th>part_program</th><th>parti_asistido</th><th>hora</th><th>idtipoevento</th><th>idmodalidad</th></tr>';
	for($i = 2; $i <=$numRows; $i++){
		$Id = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->
		getCalculatedValue();
		$nombre= $objPHPExcel->getActiveSheet()->getCell('B'.$i)->
		getCalculatedValue();
		
		$inicio= $objPHPExcel->getActiveSheet()->getCell('C'.$i)->
		getCalculatedValue();
		//$inicio= new DateTime($inicio);

		$fin= $objPHPExcel->getActiveSheet()->getCell('D'.$i)->
		getCalculatedValue();
		//$fin= new DateTime("$fin");
		$objetivo= $objPHPExcel->getActiveSheet()->getCell('E'.$i)->
		getCalculatedValue();
		$part_esp= $objPHPExcel->getActiveSheet()->getCell('F'.$i)->
		getCalculatedValue();
		if($part_esp=="")
		{
			$part_esp=0;
		}
		$part_asis= $objPHPExcel->getActiveSheet()->getCell('G'.$i)->
		getCalculatedValue();
		if($part_asis=="")
		{
			$part_asis=0;
		}
		$duracion= $objPHPExcel->getActiveSheet()->getCell('H'.$i)->
		getCalculatedValue();
		$tipoevento = $objPHPExcel->getActiveSheet()->getCell('I'.$i)->
		getCalculatedValue();
		$tipoevento=busrid_tipoevento($tipoevento);
		$modalidad= $objPHPExcel->getActiveSheet()->getCell('J'.$i)->
		getCalculatedValue();
		$modalidad=busrid_modalidad($modalidad);
		echo '<tr>';
		echo '<td>'.$Id.'</td>';
		echo '<td>'.$nombre.'</td>';
		echo '<td>'.$inicio.'</td>';
		echo '<td>'.$fin.'</td>';
		echo '<td>'.$objetivo.'</td>';
		echo '<td>'.$part_esp.'</td>';
		echo '<td>'.$part_asis.'</td>';
		echo '<td>'.$duracion.'</td>';
		echo '<td>'.$tipoevento.'</td>';
		echo '<td>'.$modalidad.'</td>';
		echo '</tr>';
		$sql = "INSERT INTO evento (idevento, eventocol, fecha_inicio, fecha_fin, objetivo, part_program, part_asistido, horas, idtipo_evento, idmodalidad) 
		VALUE($Id,'$nombre','$inicio','$fin','$objetivo','$part_esp','$part_asis','$duracion','$tipoevento','$modalidad')";
		$result = $mysqli->query($sql);
	
		//$result->free();
	}
	echo '</table>';
	function busrid_tipoevento($tipo_evento){
		require 'conexiondb.php';
		$field1name=0;
		$sql1= "SELECT * FROM tipo_evento WHERE tipo_eventocol like '$tipo_evento' ";
		$result=$mysqli->query($sql1);
		while ($row = $result->fetch_assoc()) {
			$field1name = $row["idtipo_evento"];
		}
		return $field1name;
	}

	function busrid_modalidad($modalidad_col){
		require 'conexiondb.php';
		$field1name=0;
		$sql2= "SELECT  idmodalidad  FROM modalidad WHERE UPPER(modalidadcol) = UPPER('$modalidad_col')";
		$result=$mysqli->query($sql2);
		while ($row = $result->fetch_assoc()) {
			$field1name = $row["idmodalidad"];
		}
		return $field1name;
	}
?>